# -*- coding: utf-8 -*-

import os
from ftplib import FTP

from Modules_Deployment.Version import Version
from Modules_Deployment.Deployment import Deployment
from Modules_Deployment.Mail import Mail

linkOfWebsiteOnline = "http://jeromeetval.fr/evalles/"

host = "ftp.jeromeetval.fr"
password = "Castelis123!"
username = "evalles@jeromeetval.fr"
projectName = os.path.split(os.getcwd())[1]

ftp = FTP()
port = 21
ftp.connect(host, port, 100)

print (ftp.getwelcome())    
try:
      print ("Logging in...")
      ftp.login(username, password)
except:
     "failed to login"

filenameCV = "./"


Deployment.deleteAllFiles(ftp)
Deployment.uploadFiles(ftp, filenameCV)
ftp.quit()


if Version.checkIfIsBigVersion(Version.getVersion()):
    Mail("Le déploiement du projet " + projectName + " a fonctionné !",
            "Lien du site deployé: " + linkOfWebsiteOnline + "\n"
            "Version du site: " + Version.getVersion().encode('utf-8'))
