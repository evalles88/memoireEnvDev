package fr.memoire.main;

import javax.swing.JFrame;

import fr.memoire.pages.Accueil;

public class Main {

    public static boolean main(String[] args) {
        try {
            Accueil a = new Accueil();
            a.setSize(300, 300);
            a.setVisible(true);
            a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}