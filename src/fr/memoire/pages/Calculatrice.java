package fr.memoire.pages;

import java.awt.BorderLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Calculatrice extends JFrame implements ActionListener {

    private JPanel principal = new JPanel();
    private JLabel resultat = new JLabel();
    private String operateur = "", nb2="";
    private float nb1=0.f;

    private boolean scientistMode = true ;
    JPanel operateurs = new JPanel();

    public Calculatrice() throws HeadlessException {
        initCalculatrice();
    }

    public Calculatrice(GraphicsConfiguration arg0) { super(arg0); }
    public Calculatrice(String arg0) throws HeadlessException {
super(arg0); }
    public Calculatrice(String arg0, GraphicsConfiguration arg1) {
super(arg0, arg1); }

    public boolean initCalculatrice() {
        try {
            this.add(principal);
            this.setTitle("Calculatrice");
            principal.setLayout(new BorderLayout());

            initMenuBar();

            JPanel result = new JPanel();
            setResultat("0");
            result.add(this.resultat);
            principal.add(result,  BorderLayout.NORTH);

            JPanel chiffres = new JPanel();
            principal.add(chiffres, BorderLayout.CENTER);
            chiffres.setLayout(new GridLayout(4,3));
            for (int i=0, k=1; i<4; i++) {
                for (int j=0; j<3; j++, k++) {
                    JButton button = new JButton();
                    if (k!=12)
                        button.addActionListener(new NumberListener());
                    else
                        button.addActionListener(new OperateurListener());
                    if (k==10)
                        button.setText("0");
                    else if (k==11)
                        button.setText(".");
                    else if (k==12)
                        button.setText("=");
                    else
                        button.setText(String.valueOf(k));
                    chiffres.add(button);
                }
            }

            initOperator();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean initMenuBar() {
        try {
            JMenuBar menuBar = new JMenuBar();
            this.setJMenuBar(menuBar);

            JMenu JMfichier = new JMenu("Fichier");
            menuBar.add(JMfichier);

            JMenuItem JMImode = new JMenuItem("Simple/Scientifique");
            JMImode.addActionListener(e -> initOperator() );
            JMfichier.add(JMImode);

            JMenuItem JMIconvert = new JMenuItem("Convertir");
            JMfichier.add(JMIconvert);

            JMenuItem JMIlog = new JMenuItem("Historique");
            JMfichier.add(JMIlog);

            JMenuItem JMIleave = new JMenuItem("Quitter");
            JMIleave.addActionListener(e -> System.exit(0));
            JMfichier.add(JMIleave);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean initOperator() {
        try {
            scientistMode ^= true;
            operateurs.removeAll();
            principal.add(operateurs, BorderLayout.EAST);
            if(scientistMode){
                operateurs.setLayout(new GridLayout(6,1));
            }else{
                operateurs.setLayout(new GridLayout(5,1));
            }
            JButton reset, plus, moins, mul, div;
            reset = new JButton("C");
            reset.addActionListener(new OperateurListener());
            operateurs.add(reset);

            plus = new JButton("+");
            plus.addActionListener(new OperateurListener());
            operateurs.add(plus);

            moins = new JButton("-");
            moins.addActionListener(new OperateurListener());
            operateurs.add(moins);

            mul = new JButton("*");
            mul.addActionListener(new OperateurListener());
            operateurs.add(mul);

            div = new JButton("/");
            div.addActionListener(new OperateurListener());
            operateurs.add(div);

            if(scientistMode){
                JButton square = new JButton("^");
                square.addActionListener(new OperateurListener());
                operateurs.add(square);
            }

            operateurs.repaint();
            operateurs.revalidate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getResultat() {
        return this.resultat.getText();
    }

    public void setResultat(String resultat) {
        this.resultat.setText(resultat);
    }

    public float total(float nb1, float nb2, String ope) {
        switch(ope) {
        case "+" :
            return (nb1+nb2);
        case "-" :
            return (nb1-nb2);
        case "*" :
            return (nb1*nb2);
        case "/" :
            return (nb1/nb2);
        case "^" :
            return (float) (Math.pow(nb1, nb2));
        default :
            return 0;
        }
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        // TODO Auto-generated method stub
    }

    class NumberListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String source = ((JButton) e.getSource()).getText();
            if (getResultat() == "0") {
                setResultat(source);
            } else {
                if (operateur != "")
                    nb2 = nb2.concat(source);
                setResultat(getResultat().concat(source));
            }
        }
    }

    class OperateurListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String source = ((JButton) e.getSource()).getText();
            if (source.equals("+")) {
                nb1 = Float.parseFloat(getResultat());
                setResultat(getResultat().concat("+"));
                operateur="+";
            } else if (source.equals("-")) {
                nb1 = Float.parseFloat(getResultat());
                setResultat(getResultat().concat("-"));
                operateur="-";
            } else if (source.equals("*")) {
                nb1 = Float.parseFloat(getResultat());
                setResultat(getResultat().concat("*"));
                operateur="*";
            } else if (source.equals("/")) {
                nb1 = Float.parseFloat(getResultat());
                setResultat(getResultat().concat("/"));
                operateur="/";
            } else if (source.equals("^")) {
                nb1 = Float.parseFloat(getResultat());
                setResultat(getResultat().concat("^"));
                operateur = "^";
            } else if (source.equals("=")) {
                nb1 = total(nb1, Float.parseFloat(nb2), operateur);
                setResultat(String.valueOf(nb1));
                nb2 = "";
            } else if (source.equals("C")) {
                setResultat("0");
                operateur = "";
                nb1 = 0.f;
                nb2 = "";
            }
        }
    }
}