package fr.memoire.pages;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCalculatrice {
    Calculatrice c = new Calculatrice();
    @Test
    public void testInitCalculatrice() {
        assertTrue(c.initCalculatrice());
    }

    @Test
    public void testInitMenuBar() {
        assertTrue(c.initMenuBar());
    }

    @Test
    public void testInitOperator() {
        assertTrue(c.initOperator());
    }

    @Test
    public void testTotal() {
        float f1 = 2.2f, f2 = 2.0f;
        assertTrue(4.2f == c.total(f1, f2, "+"));
        Double res = (double) Math.round(c.total(f1, f2, "-")*1000d)/1000d;
        assertTrue(0.2 == res);
        assertTrue(4.4f == c.total(f1, f2, "*"));
        assertTrue(4.84f == c.total(f1, f2, "^"));
        if (f2 != 0)
            assertTrue(1.1f == c.total(f1, f2, "/"));
        else
            fail("Division par 0");
    }
}